button = document.getElementById("button")
button.addEventListener(
    "click",
    onButtonClick
)

function onButtonClick(event){
    input = document.getElementById("input")
    number = input.value
    result = document.getElementById("result")

    /**
    if(number % 3 == 0 && number % 5 == 0) {
        resultTextNode = document.createTextNode("C")
    } else if(number % 4 == 0) {
        resultTextNode = document.createTextNode("B")
    } else if(number % 3 == 0) {
        resultTextNode = document.createTextNode("A")
    } else {
        resultTextNode = document.createTextNode(number)
    }
     */

    resultTextValue = determineResultText(number)
    resultTextNode = document.createTextNode(resultTextValue)
    result.replaceChildren(resultTextNode)
}

function determineResultText() {
    if(number % 3 == 0 && number % 5 == 0) {
        return "C"
    } else if(number % 4 == 0) {
        return "B"
    } else if(number % 3 == 0) {
        return "A"
    } else {
        return number
    }
}