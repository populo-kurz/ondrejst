
cars = [
    "Aston Martin DB12",
    "Haval Xiaolong Max",
    "Kia Concept EV5",
    "Lexus TX",
    "Mercedes-Benz CLE",
    "Peugeot Inception",
    "Zeekr X"
]

addresses = [
    "Praha 3, Jungmannova 1234/23",
    "Praha 1, Nádražní 21/41",
    "Brno, Údolní 62",
    "Brno, Dunajská 912",
    "Olomouc, 17. listopadu 735"
]

form = document.getElementById("lease_form")
form.addEventListener(
    "submit",
    onLeaseFormSubmit
)
carsSelect()
placeSelect()


function carsSelect() {
    selectParent = document.getElementById("car")
    options = cars.map(createOptionElement)
    options.map(appendCarsSelectChild)
}

// 2. bere vytvoreny <option> a pripojuje ho jako potomka do <select>
function appendCarsSelectChild(optionElement) {
    selectParent.appendChild(optionElement)
}

function onLeaseFormSubmit(event) {
    
    var formValid = true

    // vytahujeme HTML element, "schranku"
    nameInput = document.getElementById("name")
    // z elementu vytahujeme hodnotu, kterou zadal uzivatel
    nameText = nameInput.value
    if(nameText.length >= 10) {
        console.log("uzivatel zadal jmeno spravne")
    }
    else {
        console.log("uzivatel nezadal jmeno spravne")
        formValid = false
    }

    mailInput = document.getElementById("email")
    mailText = mailInput.value
    if(mailText.length >=1) {
        console.log("uzivatel zadal email spravne")
    }
    else {
        console.log("uzivatel nezadal email spravne")
        formValid = false
    }

    // pripadne 
    // if(mailText) {
    //     console.log("uzivatel zadal email spravne")
    // }
    // else {
    //    console.log("uzivatel nezadal email spravne")
    //    formValid = false
    // }

    carInput = document.getElementById("car")
    selectedCar = carInput.value
    if (selectedCar == "none") {
        console.log("uzivatel nevybral automobil")
        formValid = false
    }
    else {
        console.log("uzivatel zadal automobil spravne")
    }

    lengthInput = document.getElementById("length")
    lengthNumber = lengthInput.value
    // lengthNumber >= 3 a zaroven lengthNumber <= 30
    if (lengthNumber >= 3 && lengthNumber <= 30) {
        console.log("uzivatel zadal cislo spravne")
    }
    else {
        console.log("uzivatel nezadal cislo spravne")
        formValid = false
    }

    placeInput = document.getElementById("place")
    selectedPlace = placeInput.value
    if (selectedPlace == "none") {
        console.log("uzivatel nevybral misto")
        formValid = false
    }
    else {
        console.log("uzivatel zadal misto spravne")
    }

    if(formValid) {
        // abychom mohli zobrazit shrnuti predobnednavky, musime zamezit refreshi stranky
        event.preventDefault()

        console.log("predobjednavka bude odeslana")
        summaryDiv = document.getElementById("summary")
        
        // 1. vyrobime odstavec
        customerP = document.createElement("p")
        // 2. pak k odstavci podrazeny textovy prvek
        customerTextNode = document.createTextNode("zákazník: " + nameText)
        // 3. priradime textovy prvek k odstavci
        customerP.appendChild(customerTextNode)
        // 4. samotny odstavec priradime k <div id="summary">
        summaryDiv.appendChild(customerP)

        customerP = document.createElement("p")
        customerTextNode = document.createTextNode("vybraný automobil: " + selectedCar)
        customerP.appendChild(customerTextNode)
        summaryDiv.appendChild(customerP)

        customerP = document.createElement("p")
        customerTextNode = document.createTextNode("délka: " + lengthNumber + " dnů")
        customerP.appendChild(customerTextNode)
        summaryDiv.appendChild(customerP)

        customerP = document.createElement("p")
        customerTextNode = document.createTextNode("místo vyzvednutí: " + selectedPlace)
        customerP.appendChild(customerTextNode)
        summaryDiv.appendChild(customerP)

        customerP = document.createElement("p")
        customerTextNode = document.createTextNode("cena: " + price(lengthNumber))
        customerP.appendChild(customerTextNode)
        summaryDiv.appendChild(customerP)

    } else {
        console.log("predobjednavka nebyla odeslana, formular obsahuje chyby!")
    }
}

function price(leaseLength) {
    // HTML element <select/>
    discountSelect = document.getElementById("discount")
    // retezec, ktery drzi vybrany prvek z vyberoveho seznamu - uz konkretni value
    discountValue = discountSelect.value
    if (discountValue == "none") {
        return leaseLength * 500
    }
    else if (discountValue == "student") {
        return leaseLength * 500 * 0.8
    }
    else if (discountValue == "ztp") {
        return leaseLength * 500 * 0.5
    }
    else if (discountValue == "senior") {
        if(leaseLength <= 5) {
            return 0 // vracime nulu, protoze prvnich pet dnu je pro seniory zdarma 
        } else {
            return (leaseLength - 5) * 500 * 0.5
            // pripadne
            // daysWithDiscount = leaseLength - 5
            // return daysWithDiscount * 500 * 0.5
        }
    } else {
        console.log("Uzivatel vybral neocekavanou polozku " + discountValue)
        // pripadne dal zpracovat jako chybu
    }

}

function placeSelect() {
    placeInput = document.getElementById("place")
    options = addresses.map(createOptionElement)
    options.map(appendPlaceSelectChild)
}

function createOptionElement(itemText) {
    option = document.createElement("option")
    option.text = itemText
    option.value = itemText
    return option;
}

function appendPlaceSelectChild(optionElement) {
    // placeInput je promenna v placeSelect
    placeInput.appendChild(optionElement)
}