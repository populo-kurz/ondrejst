nextButton = document.getElementById("next")
nextButton.addEventListener(
    "click",
    onNextButtonClick
)

prevButton = document.getElementById("prev")
prevButton.addEventListener(
    "click",
    onPrevButtonClick
)

var currentImageIndex = 0

images = [
    "mercury.jpg",
    "venus.jpg",
    "earth.jpg",
    "mars.png",
    "jupiter.jpg",
    "saturn.jpg",
    "uranus.jpg",
    "neptune.jpg"
]

planets = [
    "Merkur",
    "Venuše",
    "Země",
    "Mars",
    "Jupiter",
    "Saturn",
    "Uran",
    "Neptun"
]
// given in millions of km
planetDistances = [
    57.9,
    108.2,
    149.6,
    227.9,
    778.3,
    1_400.0,
    2_900.0,
    4_500.0
]

// given in km
planetDiameters = [
    4_880,
    12_104,
    12_742,
    6_779,
    139_822,
    116_464,
    50_724,
    49_244
]

// given in days
sunOrbitalPeriod = [
    88,
    225,
    365,
    687,
    3153,
    7817,
    30660,
    60152
]

// curiosity
curiosities = [
    "Nejblíže Slunci, žhavá povrchová teplota a téměř žádná atmosféra.",
    "Silná skleníková atmosféra, extrémní teploty a husté oblaky kyseliny sírové.",
    "Jediná známá planeta s tekutou vodou, životem a unikátním magnetickým polem.",
    "Červený povrch, polární čepičky z vody a oxidu uhličitého, zkoumání možné existence mikroorganismů.",
    "Největší planeta sluneční soustavy, výrazná prstencová soustava a mnoho měsíců, včetně Galileových měsíců.",
    "Rozsáhlý prstencový systém, nižší hustota než voda, výrazná atmosféra.",
    "Nakloněná osa rotace, studená atmosféra bohatá na vodík a hélium.",
    "Modrý odstín atmosféry, silné větry, velký temný skvrnitý skvrny."
]

function onNextButtonClick(event) {
    oldIndex = currentImageIndex
    currentImageIndex = (currentImageIndex + 1) % planets.length

    mainImage = document.getElementById("main_image")
    mainImage.src = "img/" + images[currentImageIndex]

    rightImageIndex = (currentImageIndex + 1) % planets.length
    rightImage = document.getElementById("right_image")
    rightImage.src = "img/" + images[rightImageIndex]

    leftImageIndex = oldIndex
    leftImage = document.getElementById("left_image")
    leftImage.src = "img/" + images[leftImageIndex]

    updatePlanetInfo()
}

function onPrevButtonClick(event) {
    if (currentImageIndex > 0) {
        currentImageIndex = currentImageIndex - 1
    } else {
        currentImageIndex = planets.length - 1
    }

    mainImage = document.getElementById("main_image")
    mainImage.src = "img/" + images[currentImageIndex]

    rightImageIndex = (currentImageIndex + 1) % planets.length
    rightImage = document.getElementById("right_image")
    rightImage.src = "img/" + images[rightImageIndex]

    if (currentImageIndex > 0) {
        leftImageIndex = currentImageIndex - 1
    } else {
        leftImageIndex = planets.length - 1
    }

    leftImage = document.getElementById("left_image")
    leftImage.src = "img/" + images[leftImageIndex]

    updatePlanetInfo()
}

function updatePlanetInfo() {
    planetDistances = document.getElementById("planetDistances")
    planetDistanceString = "vzdálenost od Slunce: " + planetDistances[currentImageIndex] + " milionů km"
    planetDistancesTextNode = document.createTextNode(planetDistanceString)
    planetDistances.appendChild(planetDistancesTextNode)
    
    
    planetDiameters = document.getElementById("planetDiameters")
    
    
    document.getElementById("sunOrbitalPeriod")
    
    
    document.getElementById("curiosities")
}


