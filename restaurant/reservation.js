form = document.getElementById("reservationForm")
form.addEventListener(
    "submit",
    onReservationFormSubmit
    )

function onReservationFormSubmit(event){

    var formIsValid = true

    nameInput = document.getElementById("name")
    nameText = nameInput.value
    if(nameText) {
        console.log("jmeno: OK")
    } else {
        console.log("jmeno: NOK")
        formIsValid = false
    }

    sizeInput = document.getElementById("size")
    sizeNumber = sizeInput.value
    if(sizeNumber >= 1 && sizeNumber <= 8) {
        console.log("velikost: OK")
    } else {
        console.log("velikost: NOK")
        formIsValid = false
    }

    todayMilliseconds = new Date("2023-07-28").getTime()

    dateInput = document.getElementById("date")
    dateNumber = dateInput.value

    userDateMilliseconds = new Date(dateNumber).getTime()
    if(todayMilliseconds < userDateMilliseconds) {
        console.log("datum: OK")
    } else {
        console.log("datum: NOK")
        formIsValid = false
    }

    timeInput = document.getElementById("time")
    timeNumber = timeInput.value
    if(timeNumber) {
        console.log("cas: OK")
    } else {
        console.log("cas: NOK")
        formIsValid = false
    }

    if(formIsValid){
        window.open("http://www.google.com")
    } else {
        
    }
}
