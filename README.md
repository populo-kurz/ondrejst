Užitečné odkazy:
- https://www.jakpsatweb.cz/
- pro multimédia potom https://www.jakpsatweb.cz/html/audio-video.html
- specifičnost CSS selektorů https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity
- atributy pro CSS https://www.jakpsatweb.cz/css/css-vlastnosti-hodnoty-prehled.html
- pseudotřídy https://www.jakpsatweb.cz/css/pseudotridy.html