// vytahujeme element formulare z order.html
form = document.getElementById("form")
// a na nej navasujeme event listener, ktery se aktivuje v pripade stisknuti tlacitka "Dokončit"
form.addEventListener(
    "submit", // "submit" se pouziva pouze v pripade, ze mame formularove tlacitko (type="submit")
    onOrderFormSubmit
)

function onOrderFormSubmit(event) {
    var formIsValid = true

    // první způsob - spojíme všechny validace
    // if(isNameValid && isSurnameValid && ...) {
    //    window.open("index.html")
    // }

    // druhý způsob - vytvoříme oddělenou proměnnou, do které si poznačíme stav validace
    // formIsValid = isNameValid && isSurnameValid && ...
    // if(formIsValid) {
    //    window.open("index.html")
    // }

    formIsValid &&= validateName()
    formIsValid &&= validateSurname()
    formIsValid &&= validateDeliveryType()
    formIsValid &&= validateGoods()
    formIsValid &&= numberRestriction()

    if(formIsValid) {
        window.open("index.html")
    }
}
// alternativně
function validate() {
    return validateName() && validateSurname() // && ...
}

// funkce vrátí true v případě, že validace je úspěšná
function validateName() {
    nameElement = document.getElementById("name")
    nameText = nameElement.value
    if(nameText) {
        console.log("uživatel správně vyplnil jméno")
        return true
    } else {
        console.log("uživatel špatně vyplnil jméno")
        return false
    }
}

function validateSurname() {
    nameElement = document.getElementById("surname")
    nameText = nameElement.value
    if(nameText) {
        console.log("uživatel správně vyplnil příjmení")
        return true
    } else {
        console.log("uživatel špatně vyplnil příjmení")
        return false
    }
}

function deliveryTypeChoice() {
    deliveryTypeParent = findDeliveryTypeDiv()
    // vytvor <label> pro kazdy <input> ...
    deliveryElements = createDeliveryTypes()
    // ... a pripoj jej k deliveryTypeParent ve funkci nize
    appendAllDeliveryTypes(
        deliveryTypeParent,
        deliveryElements
    )
}

deliveryType = ["dhl", "balikovna", "zasilkovna", "ppl"]
function findDeliveryTypeDiv() {
    return document.getElementById("delivery_type")
}

function createDeliveryTypes() {
    input = deliveryType.flatMap(deliveryTypeElements)
    return input

    // zkraceny zapis
    // return deliveryType.map(deliveryTypeInput)
}

function deliveryTypeElements(deliveryType) {
    return [deliveryTypeInput(deliveryType), createDeliveryTypeLabel(deliveryType)]
}

function createDeliveryTypeLabel(deliveryType) {
    label = document.createElement("label")
    label.htmlFor = deliveryType
    label.innerHTML = deliveryType
    return label
}

function deliveryTypeInput(deliveryType) {
    input = document.createElement("input")
    input.type = "radio"
    // odkazujeme se na fieldset.id v html dokumentu
    input.name = "delivery_type"
    input.id = deliveryType
    input.value = deliveryType
    return input
}

function appendAllDeliveryTypes(
    deliveryTypeParent,
    deliveryTypes
) {
    deliveryTypes.map(appendDeliveryTypeChild)
}

function appendDeliveryTypeChild(deliveryType) {
    // pomoci funkce append child nove vytvoreny HTML element pripojujeme jako potomka rodice
    //    deliveryTypeParent => rodic, na ktereho napojujeme potomka
    //    deliveryType => potomek, ktereho napojujeme
    //  <deliveryTypeParent>
    //       <deliveryType>
    //  </deliveryTypeParent>
    deliveryTypeParent.appendChild(deliveryType)
}

goods = [
    "VINTAGE V6HMRSSB",
    "JET GUITARS JS 300 BK",
    "CORT A4 Plus FMMH OPN"
]


function goodsSelect() {
    selectParent = document.getElementById("goods")
    // iteruj nad goods pomoci map a pouzij funkci createGoodsOption
    options = goods.map(createGoodsOption)
    options.map(appendGoodsSelectChild)
}

function appendGoodsSelectChild(optionElement) {
    selectParent.appendChild(optionElement)
}

function createGoodsOption(itemName) {
    option = document.createElement("option")
    option.text = itemName
    option.value = itemName
    return option;
}

goodsSelect()
deliveryTypeChoice()

function numberRestriction() {
    goodsAmountInput = document.getElementById("kusy")
    goodsAmount = goodsAmountInput.value
    if (goodsAmount >= 1) {
        return true
    } else {
        console.log("uživatel zadal nesprávný počet kusů")
        return false
    }
    // pripadne
    // return goodsAmount >= 1
}
    
function validateGoods() {
    goodsElement = document.getElementById("goods")
    // pristupujeme k atributu <option>
    selectedGoods = goodsElement.value
    // pokud hodnota je "none", tak vrať false, jinak true
    // == uspeje 2 == "2", 2 == 2
    // === neuspeje 2 === "2", ale uspeje 2 === 2
    if (selectedGoods == "none") {
        console.log("uživatel nevybral zboží")
        return false
    } else {
        return true
    }
}

// pouze pro doplneni, jak muze vypadat validace <fieldset>
function validateDeliveryType() {
    deliveryTypeElement = document.getElementById("delivery_type")
    enabled = Array.from(deliveryTypeElement.children).find((c) => c.checked)
    if(enabled) {
        return true
    } else {
        console.log("uživatel nevybral způsob dopravy")
        return false
    }
}