// click   - pro bezne tlacitka
// submit  - pro osetreni odeslani formulare

counter = 1

increment = document.getElementById("increment")
increment.addEventListener(
    "click",
    // tzv. anonymni funkce
    /**
    (event) => {
        numberParagraph = document.getElementById("number")
        counter++;
        console.log(counter)
    }
    */
    handleIncrement
)


function handleIncrement(event) {
    numberParagraph = document.getElementById("number")
    counter++;
    console.log(counter)
}