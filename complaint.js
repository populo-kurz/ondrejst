form = document.getElementById("complaint_form")
form.addEventListener(
    "submit",
    onComplaintFormSubmit
)

function onComplaintFormSubmit(event) {

    var formIsValid = true

    complaintNumber = document.getElementById("complaint_no")
    complaintNumberText = complaintNumber.value

    formIsValid = validateComplaintNumber(formIsValid)

    reason = document.getElementById("reason")
    reasonText = reason.value
    if(reasonText.length >= 10) {
        console.log("uživatel správně vyplnil důvod")
    } else {
        console.log("důvod musí být vyplněn!")
        formIsValid = false
    }

    nameElement = document.getElementById("name")
    nameText = nameElement.value
    if(nameText) {
        console.log("uživatel správně vyplnil jméno")
    } else {
        console.log("jméno musí být vyplněno!")
        formIsValid = false
    }

    complaintSize = document.getElementById("complaint_size")
    sizeNumber = complaintSize.value
    if(sizeNumber >= 1) {
        console.log("uživatel zadal správný počet kusů")
    } else {
        console.log("uživatel nevyplnil správně počet kusů")
        formIsValid = false
    }

    // window.location.href = "index.html"
    if(formIsValid) {
        window.open("index.html")
    } else {
        console.log("k přesměrování nedošlo, protože uživatel vyplnil formulář správně")
    }

    // TODO ošetřit, aby uživatel do důvodu napsal alespoň 10 znaků (včetně mezer) OK
    // TODO ošetřit, aby uživatel vyplnil reklamační číslo ve formátu R-... OK
    // TODO vyplnit seznam měst dynamicky ze zadaného pole měst 
    // TODO přesměrovat uživatele na úvodní stránku v případě správně vyplněného formuláře OK
    // TODO tlačítko pro resetování formuláře
    return false
}

function validateComplaintNumber(formIsValid) {
    if (complaintNumberText.startsWith("R-")) {
        console.log("uživatel správně vyplnil první textové pole")
    } else {
        console.log("číslo objednávky musí být vyplněno!")
        formIsValid = false
    }
    return formIsValid
}

reset = document.getElementById("reset")
reset.addEventListener(
    "click",
    resetForm
)

function resetForm(event) {
    resetInput("complaint_no")
    resetInput("reason")
    resetInput("name")
    resetInput("surname")
    resetComplaintSize()

}

function resetInput(inputId) {
    reasonTextArea = document.getElementById(inputId)
    reasonTextArea.value = ""
}

function resetComplaintSize() {
    complaintSize = document.getElementById("complaint_size")
    complaintSize.value = "0"
}

cities = [
    "brno",
    "praha",
    "olomouc",
    "plzen"
]

fillInCities()

function fillInCities() {
    // 1. načteme <select>
    // 2. iterujeme přes pole cities a postupně voláním document.createElement() vytváříme jednolivé <option>
    // 3. všechny vytvořené <option> přidat <select> přidat element.appendChild()

    // 1.
    shopLocationSelect = document.getElementById("shop_location_select")
    // 2.
    options = cities.map(mapCityStringToOption)
    // 3.
    options.forEach(appendOptionToSelect)

}

function mapCityStringToOption(cityString) {
    option = document.createElement("option")
    // nastavujeme obsah, ktery se zobrazi v seznamu
    option.text = cityString
    // nastavujeme atribut value, ktery muzeme pouzit pro dalsi zpracovani
    option.value = cityString
    return option;
}

function appendOptionToSelect(option) {
    shopLocationSelect.appendChild(option)
}