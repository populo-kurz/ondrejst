form = document.getElementById("next")
form.addEventListener(
    // click davame kvuli tomu, ze je to "samostatne tlacitko" mimo formular
    "click",
    onNextButtonClick
)

function onNextButtonClick(event) {
    oldIndex = currentImageIndex
    currentImageIndex = (currentImageIndex + 1) % 10

    handleButtonClick(oldIndex, currentImageIndex)
}

form = document.getElementById("prev")
form.addEventListener(
    // click davame kvuli tomu, ze je to "samostatne tlacitko" mimo formular
    "click",
    onPrevButtonClick
    )

function onPrevButtonClick(event) {
    oldIndex = currentImageIndex
    
    if (currentImageIndex == 0) {
        currentImageIndex = images.length - 1
    }
    else {
        currentImageIndex = currentImageIndex - 1
    }

    handleButtonClick(oldIndex, currentImageIndex)
}

form = document.getElementById("rand")
form.addEventListener(
    // click davame kvuli tomu, ze je to "samostatne tlacitko" mimo formular
    "click",
    onRandomButtonClick
)

function onRandomButtonClick(event) {
    oldIndex = currentImageIndex
    currentImageIndex = getRandomArbitrary(0, 9)
    console.log(currentImageIndex)
    handleButtonClick(oldIndex, currentImageIndex)
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function handleButtonClick(oldIndex, currentImageIndex) {

    galleryTextList = document.getElementById("gallery_text")

    oldLi = galleryTextList.children[oldIndex]
    oldLi.id = ""

    // nepouzivat childNodes, ktere vraci jako potomky i komentare!
    newLi = galleryTextList.children[currentImageIndex]
    newLi.id = "highlight"

    mainImage = document.getElementById("main_image")
    mainImage.src = "img/" + images[currentImageIndex]
}

images = [
    "ahmed-yanaal-SzX8e6I4au8-unsplash.jpg", // 0
    "atanas-dzhingarov-a4Wn6M18J-w-unsplash.jpg", // 1
    "david-clode-7JrLPQXPVCI-unsplash.jpg", // 2
    "david-clode-K4vHlGVX0Hs-unsplash.jpg",
    "dusan-veverkolog-edrW8VIlJJg-unsplash.jpg",
    "jackson-eaves-m0DpCdSsuRI-unsplash.jpg",
    "jean-philippe-delberghe-n0uRXGaHesY-unsplash.jpg",
    "kevin-mueller-aeNg4YA41P8-unsplash.jpg",
    "thaaii-la-IU8sAQQyB0o-unsplash.jpg",
    "zdenek-machacek-OlKkCmToXEs-unsplash.jpg" // 9
]
var currentImageIndex = 0

function imageList(images) {
    galleryTextList = document.getElementById("gallery_text")
    items = images.map(mapImageStringToItem)
    items.forEach(appendItemToList)
}

function mapImageStringToItem(imageString) {
    item = document.createElement("li")
    textNode = document.createTextNode(imageString)
    item.appendChild(textNode)
    return item;
}

function appendItemToList(item) {
    galleryTextList.appendChild(item)
}

function highlightInitialItem() {
    galleryTextList = document.getElementById("gallery_text")
    // <li></li>
    firstItem = galleryTextList.children[0]
    // <li id="highlight"></li>
    firstItem.id = "highlight"
}

imageList(images)
highlightInitialItem()
